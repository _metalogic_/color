module bitbucket.org/_metalogic_/color

go 1.15

require (
	bitbucket.org/_metalogic_/colorable v1.0.3
	bitbucket.org/_metalogic_/isatty v1.0.4
)
